FROM python:3

COPY . .

RUN pip install celery redis

CMD [ "celery", "-A", "tasks", "worker", "--loglevel=info" ]
