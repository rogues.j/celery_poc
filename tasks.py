import time
from celery import Celery

app = Celery('tasks', broker='redis://redis:6379')

@app.task
def hello(name):
  time.sleep(10)
  return "Hello " + name


result = hello.delay("Joey")
result = hello.delay("Paul")
result = hello.delay("Joey")
result = hello.delay("Paul")
result = hello.delay("Joey")
result = hello.delay("Paul")
result = hello.delay("Joey")
result = hello.delay("Paul")
result = hello.delay("Joey")
result = hello.delay("Paul")
result = hello.delay("Joey")
result = hello.delay("Paul")
result = hello.delay("Joey")
result = hello.delay("Paul")
result = hello.delay("Joey")
result = hello.delay("Paul")
result = hello.delay("Joey")
result = hello.delay("Paul")
